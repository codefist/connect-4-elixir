defmodule Elixir4.GameServer do
  use GenServer

  @col_count 7
  @row_count 6

  @token_map %{player1: true, player2: false}

  @impl true
  def init(_second_arg_from_start_link) do
    empty_board = Enum.map(1..@col_count, fn _i -> [] end)

    {:ok, %{last_mover: nil, board: empty_board}}
  end

  # @impl true
  # def init(_) do
  #   {:ok, game_pid} = GenServer.start_link(GameServer, :ok, {@col_count, @row_count})
  #   {:ok, "x()"}
  # end

  # @impl true
  # def handle_call(:call_message, _from_process, current_state) do
  #   # new_state = %{last_move_token: "x", board_data: Map.fetch(current_state, :board_data)}
  #   new_state = Map.put(current_state, :last_move_token, "Y")
  #   {:reply, new_state, new_state}
  # end

  @impl true
  def handle_call(:autogame, _from_process, game_state) do
    final_state = game_cycle(game_state)
    {:reply, final_state, final_state}
  end

  @impl true
  def handle_call(:automove, _from, game_state) do
    available_columns = non_full_columns(game_state.board)

    # random_first_player = elem(Enum.random(@token_map), 0)
    # to choose the automatic turn

    # game_state.turn == 'player' and 'computer') || 'player'

    new_state =
      Map.delete(@token_map, game_state.last_mover)
      |> Enum.random()
      |> elem(0)
      |> make_move(Enum.random(available_columns), game_state.board)

    {:reply, new_state, new_state}
  end

  defp horizontal_winner?(board) do
    Enum.reduce(0..(@row_count - 1), false, fn row_i, acc ->
      case acc do
        true ->
          true

        false ->
          {winner_found, _} =
            Enum.reduce(0..(@col_count - 1), {false, []}, fn col_i, {winner_found, contiguous} ->
              if Enum.count(contiguous) >= 4 do
                {true, []}
              else
                new_cont =
                  case Enum.at(board, col_i) do
                    [] ->
                      []

                    non_empty_column ->
                      case Enum.at(non_empty_column, row_i) do
                        nil ->
                          []

                        this_token ->
                          case contiguous do
                            [hd | _] when hd == this_token ->
                              [this_token | contiguous]

                            [hd | _] when hd != this_token ->
                              []

                            [] ->
                              [this_token]
                          end
                      end
                  end

                {winner_found, new_cont}
              end
            end)

          winner_found
      end
    end)
  end

  defp vertical_winner?(board) do
    Enum.reduce(board, 0, fn column, acc ->
      (Enum.count(
         Enum.reduce(column, [], fn el, acc ->
           (Enum.at(acc, 0) == el and [el | acc]) || [el]
         end)
       ) >= 4 and acc + 1) || acc
    end) > 0
  end

  def has_winner?(board) do
    vertical_winner?(board) or horizontal_winner?(board)
  end

  defp game_cycle(game_state) do
    if has_winner?(game_state.board) do
      game_state
    else
      case non_full_columns(game_state.board) do
        [] ->
          game_state

        _ ->
          automove(game_state)
          |> game_cycle
      end
    end
  end

  defp automove(game_state) do
    available_columns = non_full_columns(game_state.board)

    Map.delete(@token_map, game_state.last_mover)
    |> Enum.random()
    |> elem(0)
    |> make_move(Enum.random(available_columns), game_state.board)
  end

  # @impl true
  # def handle_call({:consider_move, column, token}, _from, current_state) do
  #   Map.fetch(current_state, :board)
  #   |> non_full_columns
  # end

  def make_move(player, move_column, board) do
    updated_board =
      Enum.map(0..(@col_count - 1), fn col_i ->
        case col_i do
          _y when col_i === move_column ->
            Enum.at(board, col_i) ++ [Map.fetch!(@token_map, player)]

          _ ->
            Enum.at(board, col_i)
        end
      end)

    %{
      last_mover: player,
      board: updated_board
    }
  end

  defp non_full_columns(board) do
    Enum.reverse(
      Enum.reduce(Enum.with_index(board), [], fn {_val, index}, acc ->
        if Enum.count(Enum.at(board, index)) <= @row_count - 1 do
          [index | acc]
        else
          acc
        end
      end)
    )
  end

  # @impl true
  # def handle_call(:current_game, _from, state) do
  #   {:reply, state}
  # end

  # def current_board(pid) do
  #   {:reply, board} = GenServer.call(pid, :current_board)
  #   board
  # end
end
