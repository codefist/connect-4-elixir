defmodule Elixir4 do
  @col_count 7
  @row_count 6

  alias Elixir4.GameServer

  def start_game do
    {:ok, game_pid} = GenServer.start_link(GameServer, :ok)

    %{last_mover: last_mover, board: board} = GenServer.call(game_pid, :autogame)
    IO.puts("turn: #{last_mover}")
    print_it(board)
  end

  # def game_cycle(game_pid) do
  #   {last_mover, board} = game
  #   # print_it(board)
  #   {:ok, board} = GenServer.call(:board, game)

  #   if has_winner?(board) do
  #     IO.puts("WINNER! -> #{last_mover}")
  #   else
  #     # IO.puts("TURN #{game_state.turn}")

  #     case non_full_columns(board) do
  #       [] ->
  #         IO.puts("stalemate")

  #       available_columns ->
  #         case game_state.turn do
  #           'computer' ->
  #             # IO.puts("computer turn; available: #{inspect(available_columns)}")

  #             make_move(game_state, Enum.random(available_columns))
  #             |> game_cycle

  #           'player' ->
  #             make_move(game_state, Enum.random(available_columns))
  #             |> game_cycle
  #         end
  #     end
  #   end
  # end

  # def board_full?(_board) do
  #   false
  # end

  # def make_move(game_state, move_column) do
  #   token =
  #     case game_state.turn do
  #       'player' ->
  #         1

  #       'computer' ->
  #         2
  #     end

  #   updated_board =
  #     Enum.map(0..(@col_count - 1), fn col_i ->
  #       case col_i do
  #         _y when col_i === move_column ->
  #           Enum.at(game_state.board, col_i) ++ [token]

  #         _ ->
  #           Enum.at(game_state.board, col_i)
  #       end
  #     end)

  #   %{
  #     turn: (game_state.turn == 'player' and 'computer') || 'player',
  #     board: updated_board
  #   }
  # end

  # # def diagonal_winner?(board) do
  # # end

  # def horizontal_winner?(board) do
  #   Enum.reduce(0..(@row_count - 1), false, fn row_i, acc ->
  #     case acc do
  #       true ->
  #         true

  #       false ->
  #         {winner_found, _} =
  #           Enum.reduce(0..(@col_count - 1), {false, []}, fn col_i, {winner_found, contiguous} ->
  #             if Enum.count(contiguous) >= 4 do
  #               {true, []}
  #             else
  #               new_cont =
  #                 case Enum.at(board, col_i) do
  #                   [] ->
  #                     []

  #                   non_empty_column ->
  #                     case Enum.at(non_empty_column, row_i) do
  #                       nil ->
  #                         []

  #                       this_token ->
  #                         case contiguous do
  #                           [hd | _] when hd == this_token ->
  #                             [this_token | contiguous]

  #                           [hd | _] when hd != this_token ->
  #                             []

  #                           [] ->
  #                             [this_token]
  #                         end
  #                     end
  #                 end

  #               {winner_found, new_cont}
  #             end
  #           end)

  #         winner_found
  #     end
  #   end)
  # end

  # def vertical_winner?(board) do
  #   Enum.reduce(board, 0, fn column, acc ->
  #     (Enum.count(
  #        Enum.reduce(column, [], fn el, acc ->
  #          (Enum.at(acc, 0) == el and [el | acc]) || [el]
  #        end)
  #      ) >= 4 and acc + 1) || acc
  #   end) > 0
  # end

  # def has_winner?(board) do
  #   vertical_winner?(board) or horizontal_winner?(board)
  # end

  def print_it(board) do
    # step through a conceptual 2d grid
    # step through List(of Lists) until positions match conceptual then print to screen if value found
    (@row_count - 1)..0
    |> Enum.each(fn row ->
      Enum.each(0..(@col_count - 1), fn col ->
        column = Enum.at(board, col)

        case Enum.at(column, row) do
          true ->
            IO.write("|R|")

          false ->
            IO.write("|B|")

          nil ->
            IO.write("|-|")
        end
      end)

      IO.puts("")
    end)
  end

  # Leaving the player input logic here for now
  #
  # IO.puts("player turn; available: #{inspect(non_full_columns(game_state.board))}")
  # user_input = IO.gets("make a move (a 0-index column number): ")
  # {user_move, _} = Integer.parse(hd(Regex.run(~r/[0-9]+/, user_input)))

  # if Enum.member?(0..(@col_count - 1), user_move) do
  #   if Enum.count(Enum.at(game_state.board, user_move)) >= @row_count do
  #     IO.puts("This column is full, try again")
  #     game_cycle(game_state)
  #   else
  #     make_move(game_state, user_move)
  #     |> game_cycle
  #   end
  # else
  #   IO.puts("only 0 to #{@col_count - 1} is valid")
  #   game_cycle(game_state)
  # end
end
